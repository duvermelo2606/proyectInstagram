package com.example.proyectinstagram

import androidx.compose.animation.*
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.SensorDoor
import androidx.compose.runtime.Composable
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import kotlin.random.Random.Default.nextInt

@Composable
fun ColorAnimationSimple() {
    var firtsColor by rememberSaveable {
        mutableStateOf(false)
    }
    var showBox by rememberSaveable {
        mutableStateOf(true)
    }
    val realFirtsColor by animateColorAsState(
        targetValue = if (firtsColor) Color.Blue else Color.Green,
        animationSpec = tween(durationMillis = 2000),
        finishedListener = { showBox = false }
    )
    if (showBox) {
        Box(
            modifier = Modifier
                .size(100.dp)
                .background(realFirtsColor)
                .clickable { firtsColor = !firtsColor })
    }
}

@Composable
fun SizeAnimation() {
    var smallSize by rememberSaveable {
        mutableStateOf(true)
    }
    val size by animateDpAsState(
        targetValue = if (smallSize) 50.dp else 100.dp,
        animationSpec = tween(durationMillis = 3000), finishedListener = {
            if (!smallSize) {
            }
        }
    )

    Column {

        Box(modifier = Modifier
            .size(size)
            .background(Color.Red)
            .clickable { smallSize = !smallSize })
    }
}

@Composable
fun VisibilityAnimation() {
    var visibility by rememberSaveable {
        mutableStateOf(false)
    }

    val textButton = if (visibility) "Ocultar" else "Mostrar"


    Column(Modifier.fillMaxSize()) {
        Button(onClick = { visibility = !visibility }) {
            Text(text = "$textButton")
        }
        Spacer(modifier = Modifier.size(50.dp))
        AnimatedVisibility (visibility, enter = slideInHorizontally(), exit = slideOutHorizontally()) {
            Box(
                modifier = Modifier
                    .size(150.dp)
                    .background(Color.Red)
            )
        }
    }
}

@Composable
fun CrossfadeExampleAnimation() {
    var myComponentType: ComponentType by remember {
        mutableStateOf(ComponentType.Text)
    }
    Column(modifier = Modifier.fillMaxSize()) {
        Button(onClick = { myComponentType = getComponentTypeRandom() }) {
            Text(text = "Cambiar componente")
        }
        Crossfade(targetState = myComponentType) {myComponentType ->
            when (myComponentType) {
                ComponentType.Image -> Icon(Icons.Default.SensorDoor, contentDescription = "Sensordoor")
                ComponentType.Text -> Text(text = "Compoenente random")
                ComponentType.Box -> Box(
                    modifier = Modifier
                        .size(150.dp)
                        .background(Color.Red)
                )
                ComponentType.Error -> Text(text = "Error")
            }
        }

    }
}

enum class ComponentType() {
    Image, Text, Box, Error
}

fun getComponentTypeRandom(): ComponentType {
    return when(nextInt(from = 0, until = 3)) {
        0 -> ComponentType.Image
        1 -> ComponentType.Text
        2 -> ComponentType.Box
        else -> ComponentType.Error
    }
}